from enum import Enum

from misc import Root


class Equality(Enum):
    NOT_EQUAL = -1
    UNKNOWN = 0
    EQUAL = 1


class SolvingMatrix(Root):
    class MatrixRow(Root):
        def __init__(self, key, matrix, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.key = key
            self.matrix = matrix

        def __getitem__(self, key2):
            try:
                return self.matrix[self.key][key2]
            except KeyError:
                try:
                    return self.matrix[key2][self.key]
                except KeyError:
                    return None

        def __setitem__(self, key, value):
            if self.key in self.matrix and key in self.matrix:
                if len(self.matrix[self.key]) > len(self.matrix[key]):
                    key1, key2 = self.key, key
                else:
                    key1, key2 = key, self.key
            elif self.key in self.matrix:
                key1, key2 = self.key, key
            else:
                key1, key2 = key, self.key
            if key2 in self.matrix[key1]:
                self.matrix[key1][key2] = value

        def __iter__(self):
            if self.key in self.matrix:
                for intersect, intersect_value in self.matrix[self.key].items():
                    yield (intersect, intersect_value)
            for key, entry in [(key, entry) for key, entry
                               in self.matrix.items() if key != self.key]:
                yield from [(key, v) for k, v in entry.items() if k == self.key]

    def __init__(self, nouns, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.categories = {}
        self.matrix = {}
        self.init_matrix(nouns)

    def init_matrix(self, nouns: dict):
        for category in nouns.keys():
            self.categories[category] = set(nouns[category])
        categories_left = list(self.categories.keys())
        while len(categories_left) > 1:
            category = categories_left.pop()
            for subject in nouns[category]:
                self.matrix[subject] = {}
                for other_category in categories_left:
                    for other_subject in nouns[other_category]:
                        self.matrix[subject][other_subject] = Equality.UNKNOWN

    def __getitem__(self, item):
        """
        item_intersects = {}
        if item in self.matrix:
            for intersect, intersect_value in self.matrix[item].items():
                item_intersects[intersect] = intersect_value
        for key, entry in [(key, entry) for key, entry
                           in self.matrix.items() if key != item]:
            test1 = {key: v for k, v in entry.items() if k == item}
            item_intersects.update(test1)
        return item_intersects
        """
        return self.MatrixRow(item, self.matrix)

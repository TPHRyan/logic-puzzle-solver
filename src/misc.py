class Root(object):
    def __init__(self, *args, **kwargs):
        super().__init__()

    def __getattr__(self, item):
        assert not hasattr(super(), item)
        return self.do_nothing

    def do_nothing(self, *args, **kwargs):
        pass

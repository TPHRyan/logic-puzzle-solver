from collections import namedtuple
from re import compile


Rule = namedtuple('Rule', ['type', 'subjects'])


PRIMARY_PATTERNS = {
    compile('^[Oo]f'): 'a_and_b_are_c_and_d',
    compile('[^Nn]either'): 'a_is_b_or_c',
    compile('doesn\'t'): 'single_not',
    compile('^[Nn]either'): 'double_not',
}

ORDERED_PATTERNS = {
    compile('less'): 'a_less_b',
    compile('more'): 'a_more_b',
    compile('before'): 'a_less_b',
    compile('after'): 'a_more_b',
    compile('younger'): 'a_less_b',
    compile('older'): 'a_more_b',
}

TIERS = {
    1: {
        'single_not',
        'double_not',
    },
    2: {
        'a_and_b_are_c_and_d',
        'a_is_b_or_c',
    },
    3: {
        'a_more_b',
        'a_less_b',
    },
    4: {
        'a_is_b'
    }
}

DEFAULT_RULE = 'a_is_b'


def find_in_pattern_set(phrase: str, patset: dict):
    matches = []
    for pat, rule in patset.items():
        if pat.search(phrase):
            matches.append(rule)
    return matches


def find_possible_rules(phrase: str):
    matches = find_in_pattern_set(phrase, PRIMARY_PATTERNS)
    if matches:
        return set(matches)
    matches = find_in_pattern_set(phrase, ORDERED_PATTERNS)
    if matches:
        return set(matches)
    return {DEFAULT_RULE}


def find_probable_rule(possibilities: set):
    for tier in sorted(TIERS.keys()):
        probs = []
        for rule in TIERS[tier]:
            if rule in possibilities:
                probs.append(rule)
        if len(probs) > 1:
            raise RuntimeError('Multiple probable rules found for clue!')
        elif len(probs) == 1:
            return probs[0]
    raise RuntimeError('No rules matched probabilities for a possible rule!'
                       '{}'.format(rule))


def convert_rule(rule: Rule, quantities):
    if rule.type not in ('a_more_b', 'a_less_b'):
        return rule
    subs = rule.subjects[:]
    if rule.type == 'a_less_b':
        subs[0], subs[-1] = subs[-1], subs[0]
    if len(quantities) > 0:
        subs = subs[:1] + quantities[:1] + subs[1:]
    if len(subs) > 2:
        new_type = 'a_n_more_b'
    else:
        new_type = 'a_some_more_b'
    return Rule(new_type, subs)

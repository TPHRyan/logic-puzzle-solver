import re

from misc import Root

USELESS_WORDS = ['and', 'done', 'from', 'has', 'in', 'is', 'other', 'previous',
                 'that', 'The', 'the', 'who', ]


class NounProcessor(Root):
    def __init__(self, *args, nouns=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.nouns = {} if nouns is None else nouns

    def find_noun(self, s: str):
        noun_loc = -1
        for noun_list in self.nouns.values():
            for noun in noun_list:
                noun_loc = s.find(noun)
                if noun_loc != -1:
                    return noun, noun_loc
        return None, noun_loc

    def tokenize_nouns(self, s: str):
        if s.replace(' ', '') == '':
            return []
        noun, noun_loc = self.find_noun(s)
        if noun:
            return (self.tokenize_nouns(s[:noun_loc]) +
                    [noun] +
                    self.tokenize_nouns(s[noun_loc+len(noun):]))
        return [s.strip()]

    def separate_nouns_and_other(self, phrases: list):
        nouns = []
        others = []
        for phrase in phrases:
            if self.find_noun(phrase)[0]:
                nouns.append(phrase)
            else:
                others.append(phrase)
        return nouns, ' '.join(others)


class SecondaryString(Root):
    def __init__(self, secondary_string, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.string = secondary_string

    def cents_to_dollars(self):
        collapsed = self.string.split(' ')
        try:
            cent_i = collapsed.index('cents')
        except ValueError:
            return self
        else:
            cent_amount = int(collapsed[cent_i-1]) / 100
            collapsed[cent_i-1] = '${:.2f}'.format(cent_amount)
            del collapsed[cent_i]
        self.string = ' '.join(collapsed)
        return self

    def find_quantities(self):
        quantities = []
        for match in re.finditer('[0-9]+([\.,][0-9]*)?', self.string):
            quantities.append(match.group(0))
        return quantities

    def remove_unnecessary_words(self):
        working_string = self.string.replace('.', '')
        for word in USELESS_WORDS:
            word = '(\s|^){}([\s\.]|$)'.format(word)
            working_string = re.sub(word, ' ', working_string)
            working_string = working_string.replace('  ', ' ')
        self.string = working_string
        return self

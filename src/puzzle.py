import json

from misc import Root


class LogicPuzzle(Root):
    @classmethod
    def from_json(cls, filename: str):
        with open(filename) as f:
            puzzle_dict = json.load(f)
        num_categories = len(puzzle_dict['subjects'])
        num_subjects = len(
                puzzle_dict['subjects'][list(puzzle_dict['subjects'].keys())[0]]
        )
        puzzle_size = '{}x{}'.format(num_categories, num_subjects)
        clues = [clue[3:] for clue in puzzle_dict['clues']]
        return cls(puzzle_size, clues=clues, subjects=puzzle_dict['subjects'])

    def __init__(self, puzzle_size: str, *args,
                 clues=None, subjects: dict=None, **kwargs):
        super().__init__(*args, **kwargs)
        num_categories, num_subjects = map(int, puzzle_size.split('x'))
        if subjects is None:
            raise ValueError('No subjects provided for puzzle!')
        assert(len(subjects) == num_categories)
        for category in subjects.values():
            assert(len(category)) == num_subjects
        self.clues = [] if clues is None else clues
        self.subjects = subjects

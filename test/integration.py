from parser import NounProcessor
from parser import SecondaryString
from parser.rules import Rule
from parser.rules import convert_rule
from parser.rules import find_possible_rules
from parser.rules import find_probable_rule
from puzzle import LogicPuzzle

puzzle = LogicPuzzle.from_json('files/puzzle.json')
test_clues = puzzle.clues
test_nouns = puzzle.subjects

for i, clue in enumerate(test_clues, start=1):
    noun_proc = NounProcessor(nouns=test_nouns)
    nouns, other = noun_proc.separate_nouns_and_other(
            noun_proc.tokenize_nouns(clue)
    )
    secondary = SecondaryString(other)
    quantities = secondary.cents_to_dollars().find_quantities()
    secondary = secondary.remove_unnecessary_words()
    possibilities = find_possible_rules(secondary.string)
    rule = Rule(find_probable_rule(possibilities), nouns)
    rule = convert_rule(rule, quantities)
    print('{}: {}'.format(i, str(rule)))

